## ELK playground

This is a playground to explore the capabilities of Elasticsearch, Logstash and Kibana. Mostly to experiment

## Getting started

```shell
bin/setup
bin/start
```

## Create dashboards

https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html

## Load dashboards and indexes

```
git clone https://github.com/elastic/beats.git
cd beats
dev-tools/import_dashboards.sh -d ./topbeat/etc/kibana -l http://192.168.64.5:9200
```

## Start topbeat

```
./topbeat -e -c topbeat.yml -d "publish"
```

## Further links

 - https://grokdebug.herokuapp.com/
